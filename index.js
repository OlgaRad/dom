const container = document.querySelector('.container');
const btn_by_date = document.querySelector('.btn_by_date');
const btn_by_episode = document.querySelector('.btn_by_episode')
let allEpisodes = []
console.log(allEpisodes);

const url = 'https://rickandmortyapi.com/api/character';
fetch(url)
    .then(res => res.json())
    .then(data => {
        const rawData = data.results;
        // console.log(rawData)

        // return rawData.map(character => {
        //   //all needed data is listed below as an entity 
        //    let created = character.created;
        //    let species = character.species ;
        //    let img = character.image;
        //    let episodes = character.episode;
        //    let name = character.name;
        //    let location = character.location;
        //     //create element
        //     //append element
        //     // console.log(data.results);
        //   })
        rawData.forEach(item => {
            if (item.name) {
                const card = document.createElement('li');
                card.classList.add('card');
                container.append(card);
                const heroName = document.createElement('div');
                heroName.classList.add('heroName');
                heroName.innerText = item.name;
                card.append(heroName);
                const heroImage = document.createElement('img');
                heroImage.setAttribute("src", item.image);
                heroImage.classList.add('heroImage');
                card.append(heroImage);
                const heroSpecies = document.createElement('div');
                heroSpecies.classList.add('heroSpecies');
                heroSpecies.innerText = item.species;
                card.append(heroSpecies);
                const heroLocation = document.createElement('div');
                heroLocation.classList.add('heroLocation');
                heroLocation.innerText = item.location.name;
                card.append(heroLocation);
                const heroCreated = document.createElement('div');
                heroCreated.classList.add('heroCreated');
                heroCreated.innerText = item.created;
                card.append(heroCreated);
                const listOfEpisodes = document.createElement('select');
                listOfEpisodes.classList.add('listOfEpisodes');
                card.append(listOfEpisodes);
                const episode = document.createElement('option');
                episode.innerText = "Episodes:";
                listOfEpisodes.append(episode);
                if (item.episode) {
                    let arrEpisodes = [];

                    item.episode.forEach(item => {
                        fetch(item)
                            .then(response => response.json())

                            .then(res => {
                                const newEpisode = document.createElement('option')
                                newEpisode.innerText = res.episode;
                                listOfEpisodes.append(newEpisode);
                                arrEpisodes.push(res.episode);
                            })
                    })
                    allEpisodes.push(arrEpisodes);
                }
            }
        })
    });


function sortListByDate(ul) {
    let new_ul = ul.cloneNodes(false);
    let arrList = [];
    for (let i = ul.childNodes.length; i--;) {
        if (ul.childNodes[i].nodeName === 'Li')
            arrList.push(ul.childNodes[i]);
    }

    arrList.sort(function(a, b) {
        let dateA = document.querySelector('.heroCreated').innerText;
        console.log(dateA)
        let dateB = document.querySelector('.heroCreated').innerText;
        if (Data.parse(dateA) < Data.parse(dateB)) {
            console.log(Data.parse(dateA))
            return Data.parse(dateA) - Data.parse(dateB)
        } else if (Data.parse(dateA) > Data.parse(dateB)) {
            return Data.parse(dateB) - Data.parse(dateA)
        } else
            return 0;
    })

    for (let i = 0; i < arrList.length; i++) {
        new_ul.appendChild(arrList[i]);
        ul.parentNode.replaceChild(new_ul, ul);
    }

}

function sortLis() {
    let switching = true;
    while (switching) {
        switching = false;
        let tag_ul = document.querySelector(".container");
        let tag_li = tag_ul.getElementsByTagName("li");
        for (let i = 0; i < tag_li.length; i++) {
            let childElements = tag_li[i].childNodes;
            console.log(childElements);
            for (let j = 0; j < childElements.length - 1; j++) {
                let divName = childElements[4].innerText;
                console.log(divName)
            }
        }
    }
}
sortLis();


function showMatch() {
    let selectElem = document.querySelector('.sort');
    let arrOptions = selectElem.childNodes;
    console.log(arrOptions)
    for (let i = 0; i < arrOptions.length; i++) {
        if (arrOptions[i].innerText === 'ascending') {
            sortListByDate(document.getElementByClassName('heroCreated')[0]);
        } else if (arrOptions[i].innerText === 'descending') {
            sortListByDate(document.getElementByClassName('heroCreated')[0]);
        }
    }
}

// function sortByEpisode() {

// }

btn_by_date.addEventListener('click', showMatch);
// btn_by_episode.addEventListener('click', );

function myFunction() {
    const input = document.getElementById("mySearch");
    const filter = input.value.toUpperCase();
    let tag_ul = document.querySelector(".container");
    let tag_li = tag_ul.querySelectorAll("li.card");
    for (let i = 0; i < tag_li.length; i++) {
        let childElements = tag_li[i].childNodes;
        // console.log(childElements);
        for (let j = 0; j < childElements.length - 1; j++) {
            let divName = childElements[0].innerText;
            if (divName.toUpperCase().indexOf(filter) > -1) {
                tag_li[i].style.display = "";
            } else {
                tag_li[i].style.display = "none";
            }
        }
    }
}